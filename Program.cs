﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_24
{
    class Program
    {
        static void Main(string[] args)
        {
            Book book1 = new Book("hi", "my", "Naor", "action");
            Book book2 = new Book("hello", "not mine", "Azar", "comedy");
            Book book3 = new Book("shalom", "yours", "Israel", "romantic comedy");
            MyLibrary library = new MyLibrary();
            library.AddBook(book1);
            library.AddBook(book2);
            library.AddBook(book3);
            Console.WriteLine(library);
            Console.WriteLine();
            Console.WriteLine(library.AddBook(book1));
            Console.WriteLine();
            //Console.WriteLine(library.RemoveBook("hi"));
            Console.WriteLine(library.HaveThisBook("shalom"));
            Console.WriteLine();
            Console.WriteLine(library.GetBook("hello"));
            Console.WriteLine();
            Console.WriteLine(library.GetBookByAuthor("Naor"));
            Console.WriteLine();
            library.GetBooksTitleSorted();
            Console.WriteLine();
            Console.WriteLine(library.Count);
        }
    }
}
