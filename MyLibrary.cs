﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_24
{
    class MyLibrary
    {
        private Dictionary<string, Book> books = new Dictionary<string, Book>();

        public MyLibrary()
        {

        }

        public bool AddBook(Book book)
        {
            if (books.ContainsKey(book.Title))
            {
                return false;
            }
            else
            {
                books.Add(book.Title, book);
                return true;
            }
        }
        public bool RemoveBook(string key)
        {
            if (books.ContainsKey(key))
            {
                books.Remove(key);
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool HaveThisBook(string key)
        {
            if (books.ContainsKey(key))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public Book GetBook(string key)
        {
            if (books.ContainsKey(key))
            {
                return books[key];
            }
            else
            {
                return null;
            }
        }
        public Book GetBookByAuthor(string author)
        {
            Book the_book = null;
            foreach (KeyValuePair<string,Book> authors in books)
            {
                if (authors.Value.Author == author)
                {
                    the_book = authors.Value;
                }
            }
            return the_book;
        }
        public void Clear()
        {
            books.Clear();
        }
        public List<string> GetAuthors()
        {
            List<string> authors = new List<string>(books.Count);
            foreach (KeyValuePair<string,Book> smart_people in books)
            {
                authors.Add(smart_people.Value.Author);
            }
            return authors;
        }
        public List<Book> GetBooksSortedByAuthorName()
        {
            List<Book> authors_sorted = new List<Book>();
            foreach (KeyValuePair<string,Book> pair in books)
            {
                authors_sorted.Add(pair.Value);
            }
            authors_sorted = authors_sorted.OrderBy(o => o.Author).ToList();
            return authors_sorted;
        }
        public List<string> GetBooksTitleSorted()
        {
            List<string> title_book = new List<string>();
            foreach (KeyValuePair<string,Book> pair in books)
            {
                title_book.Add(pair.Value.Title);
            }
            title_book.Sort();
            return title_book;
        }
        public int Count
        {
            get
            {
                return books.Count();
            }
        }

        public override string ToString()
        {
            foreach (KeyValuePair<string,Book> book in books)
            {
                Console.WriteLine("book name: " + book.Value.Title);
            }
            return null;
        }
    }
}
